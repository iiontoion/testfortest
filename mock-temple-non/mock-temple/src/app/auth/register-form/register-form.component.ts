import { Component, OnInit, Input, ViewChild, Renderer, EventEmitter, Output } from '@angular/core';
import { MenuItem, MessageService, ConfirmationService } from 'primeng/api';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { formatDate } from '@angular/common';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TitleNameService } from 'src/app/shared/service/title-name.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ManageUserService } from 'src/app/shared/service/manage-user.service';
import { AuthService } from '../../shared/service/auth.service';
import { BreadcrumbService } from 'src/app/shared/service/breadcrumb.service';
import { SelectItem } from 'primeng/api';
interface Transportation {
  memberTransportation: string;
}
@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  public formRegister: FormGroup;
  public menu: MenuItem[];
  public titleName: any[];
  public th: any;
  public yearRange: string;
  public detailWarning: string;
  public registerSuccess: boolean;
  public showCancelMessage: boolean;
  public urlback: string;
  public messageback: string;
  @ViewChild('uploader') fileInput;
  @Output() imageChange = new EventEmitter();
  image: string;
  transportation: SelectItem[];
  selectedTransportation: string;
  tatlename: SelectItem[];
  selectedtatlename: string;
  public formError = {
    username: '',
    password: '',
    repassword: '',
    titleName: '',
    fname: '',
    lname: '',
    memberJob: '',
    gender: '',
    address: '',
    phone: '',
    email: '',
    phoneEmergency: '',
    memberTransportation: '',
    memberTatlename: '',
    // other: '',
    memberCoursePassed: '',
    membermemberExpected: '',

    memberExpPassed: '',
    memberNote: '',
  };

  public validationMessage = {
    username: {
      datail: 'กรุณากรอก Username',
      required: 'Username*'
    },
    password: {
      datail: 'กรุณากรอก Password',
      required: 'Password*'
    },
    repassword: {
      datail: 'กรุณากรอก Re-password',
      required: 'Re-password*'
    },
    titleName: {
      datail: 'กรุณากรอก คำนำหน้า',
      required: 'คำนำหน้า*'
    },
    fname: {
      datail: 'กรุณากรอก ชื่อ',
      required: 'ชื่อ*'
    },
    lname: {
      datail: 'กรุณากรอก นามสกุล',
      required: 'นามสกุล*'
    },
    memberJob: {
      datail: 'กรุณากรอก อาชีพ',
      required: 'อาชีพ*'
    },
    gender: {
      datail: 'กรุณากรอก เพศ',
      required: 'เพศ*'
    },
    address: {
      datail: 'กรุณากรอก ที่อยู่',
      required: 'ที่อยู่*'
    },
    phone: {
      datail: 'กรุณากรอก เบอร์โทร',
      required: 'เบอร์โทร*'
    },
    email: {
      datail: 'กรุณากรอก E-mail',
      required: 'E-mail*'
    },
    phoneEmergency: {
      datail: 'กรุณากรอก เบอร์ติดต่อฉุกเฉิน',
      required: 'เบอร์ติดต่อฉุกเฉิน*'
    },

    memberExpected: {
      datail: 'กรุณากรอก ความคาดหวังในครั้งนี้',
      required: 'ความคาดหวังในครั้งนี้*'
    },
    memberCoursePassed: {
      datail: 'กรุณากรอก จำนวนคอร์สที่เคยปฎิบัติธรรมที่วัดโสมพนัส',
      required: 'จำนวนคอร์สที่เคยปฎิบัติธรรมที่วัดโสมพนัส*'
    }, memberExpPassed: {
      datail: 'กรุณากรอก กรุณาเล่าประสบการณ์ปฎิบัติธรรมที่ผ่านมา',
      required: 'กรุณาเล่าประสบการณ์ปฎิบัติธรรมที่ผ่านมา*'
    },
    memberNote: {
      datail: 'กรุณากรอก หมายเหตุอื่นๆโรคประจำตัว',
      required: 'หมายเหตุอื่นๆโรคประจำตัว*'
    },
    // other: {
    //   datail: 'กรุณากรอก การเดินทางมาที่วัด',
    //   required: 'การเดินทางมาที่วัด*'
    // },
    memberTransportation: {
      datail: 'กรุณากรอก การเดินทางมาที่วัด',
      required: 'การเดินทางมาที่วัด*'
    }
  }



  constructor(
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private titleService: TitleNameService,
    private router: Router,
    private manageUserService: ManageUserService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private breadCrumbService: BreadcrumbService,
    private renderer: Renderer
  ) {

    this.transportation = [
      { label: 'รถยนต์ส่วนตัว', value: 'รถยนต์ส่วนตัว' },
      { label: 'รถโดยสารประจำทาง', value: 'รถโดยสารประจำทาง' },
      { label: 'รถมอเตอร์ไซค์', value: 'รถมอเตอร์ไซค์' },
      { label: 'เดิน', value: 'เดิน' }

    ];

  }



  ngOnInit() {
    this.breadCrumbService.setPath([
      { label: 'Members management : จัดการสมาชิกทั้งหมด', routerLink: '/users' },
      { label: 'Create members : ลงทะเบียนสมาชิก', routerLink: '/users/create' },
    ]);

    this.urlback = this.route.snapshot.data.urlback;
    this.registerSuccess = false;
    this.showCancelMessage = false;
    this.setBack();
    this.createForm();
    this.settingCalendarTH();
    this.titleService.getTitleNames().subscribe(
      res => {
        console.log(res);

        this.titleName = [
          ...res
        ];
        console.log(this.titleName);

      },
      err => {
        console.log(err['error']['message']);
      }
    )

    this.menu = [
      { label: 'Login', url: 'auth/login' },
      { label: 'Register : สมัครสมาชิก' },
    ];
    this.image = '/assets/images/no-img.png';

  }

  setBack() {
    this.urlback = this.route.snapshot.data.urlback;
    this.messageback = this.route.snapshot.data.messageback;
  }
  triggerFileUpload() {
    this.renderer.invokeElementMethod(
      this.fileInput.nativeElement, 'dispatchEvent', [new MouseEvent('click', { bubbles: true })])

  }
  uploadImage(event) {
    const reader = new FileReader();
    const image = event.target.files[0];

    reader.onload = () => {
      const result = reader.result;

      this.image = result as string;
      this.imageChange.emit(result.toString());
    }
    reader.readAsDataURL(image);
  }

  createForm() {
    this.formRegister = this.formBuilder.group(
      {
        username: ['', [Validators.required, Validators.minLength(5)]],
        password: ['', [Validators.required, Validators.minLength(5)]],
        repassword: ['', [Validators.required, Validators.minLength(5)]],
        titleName: ['', Validators.required],
        fname: ['', Validators.required],
        lname: ['', Validators.required],
        memberJob: ['', Validators.required],
        gender: ['', Validators.required],
        address: ['', Validators.required],
        phone: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        phoneEmergency: ['', Validators.required],
        memberExpected: ['', Validators.required],
        memberCoursePassed: ['', Validators.required],
        memberNote: ['', Validators.required],
        // other: ['', Validators.required],
        memberTransportation: ['', Validators.required],
        memberExpPassed: ['', Validators.required],
        image: ''

      }
    );
  }

  settingCalendarTH() {
    this.th = {
      firstDayOfWeek: 1,
      dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
      monthNames: ['มกราคม ', 'กุมภาพันธ์ ', 'มีนาคม ', 'เมษายน ',
        'พฤษภาคม  ', 'มิถุนายน ', 'กรกฎาคม ', 'สิงหาคม ',
        'กันยายน ', 'ตุลาคม ', 'พฤศจิกายน ', 'ธันวาคม '],
      today: 'Today',
      clear: 'Clear',
    };

    const currentYear = formatDate(new Date(), 'yyyy', 'en');
    const startYear = parseInt(currentYear) - 100;
    this.yearRange = startYear + ':' + currentYear;
  }

  onSubmit(e) {
    console.log(this.formRegister.valid);
    if (!this.formRegister.valid) {
      this.subscribeInputMessageWaring();
      this.showMessage();
    } else {
      this.submitMessage(e);
    }
    //console.log('test');

  }

  submitMessage(e) {
    const message = "ยืนยันการสมัครสมาชิก";
    const type = "submit"
    this.showDialog(message, type);
  }


  showDialog(message, type) {
    this.confirmationService.confirm({
      message: message,
      header: 'ข้อความจากระบบ',
      accept: () => {
        this.actionAccept(type);
      },
      reject: () => {
      }
    });
  }

  actionAccept(type) {
    switch (type) {
      case "cancle": {
        this.router.navigateByUrl(this.urlback);
        break;
      }
      case "submit": {
        console.log("submit");
        const titleCode = this.formRegister.get('titleName').value;
        const dataUser = {
          username: this.formRegister.get('username').value,
          password: this.formRegister.get('password').value,
          fname: this.formRegister.get('fname').value,
          lname: this.formRegister.get('lname').value,
          memberJob: this.formRegister.get('memberJob').value,
          memberTransportation: this.formRegister.get('memberTransportation').value,
          // other: this.formRegister.get('other').value,

          memberExpected: this.formRegister.get('memberExpected').value,
          memberCoursePassed: this.formRegister.get('memberCoursePassed').value,
          memberNote: this.formRegister.get('memberNote').value,
          memberExpPassed: this.formRegister.get('memberExpPassed').value,

          address: this.formRegister.get('address').value,
          tel: this.formRegister.get('phone').value,
          emergencyTel: this.formRegister.get('phoneEmergency').value,
          email: this.formRegister.get('email').value,
          img: null,
          registerDate: null,
          lastUpdate: null,
          genderId: this.formRegister.get('gender').value,
          titleId: parseInt(titleCode.id),
        };

        this.manageUserService.createUser(dataUser).subscribe(
          res => {
            if (res['status'] === 'Success') {
              this.showToast("alertMessage", "สมัครสมาชิกสำเร็จ")
            } else {
              this.showToast("alertMessage", "สมัครสมาชิกไม่สำเร็จ")
            }
          },
          err => {
            console.log("submit error");
            console.log(err);
          }
        );

        break;
      }
      default: { break; }
    }
  }
  CheckColors(val) {
    var element = document.getElementById('color');
    if (val == 'pick a color' || val == 'others')
      element.style.display = 'block';
    else
      element.style.display = 'none';
  }
  showMessage() {
    this.showToast("systemMessage", this.detailWarning);
  }

  onReject() {
    if (this.registerSuccess) {
      this.router.navigateByUrl(this.urlback);
    }
    this.messageService.clear('systemMessage');
    this.showCancelMessage = false;
  }

  showCancel() {
    //this.showMessage('cancel');
    const message = "ยกเลิกการสมัคร ?";
    const type = "cancle";
    this.showDialog(message, type);
  }


  profileSelect(e) {
    console.log(e.files);
  }

  subscribeInputMessageWaring() {
    this.formRegister
      .valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(() => this.waringMessage());
    this.waringMessage();
  }

  waringMessage() {
    if (!this.formRegister) {
      return;
    }
    this.detailWarning = '';
    for (const field of Object.keys(this.formError)) {
      this.formError[field] = '';
      const control = this.formRegister.get(field);
      if (field === 'repassword' && control.value !== '' && control.value !== this.formRegister.get('password').value) {
        this.detailWarning += 'กรุณากรอก รหัสผ่านให้ตรงกัน' + '\n';
        this.formRegister.controls[field].setValue('');
        this.formError[field] = this.validationMessage[field].required;
      } else if (control && !control.valid) {
        this.detailWarning += this.validationMessage[field].datail + '\n';
        this.formError[field] = this.validationMessage[field].required;
      }
    }

  }

  showToast(key, detail) {
    this.messageService.clear();
    this.messageService.add(
      {
        key: key,
        sticky: true,
        summary: 'ข้อความจากระบบ',
        detail: detail
      }
    );
  }

}
