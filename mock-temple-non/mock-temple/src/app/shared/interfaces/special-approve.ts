export interface SpecialApprove {
  specialApproveId?: number;
  courseId?: number;
  memberId?: number;
  detail?: string;
  status?: string;
  createDate?: Date;
  lastUpdate?: Date;
  courseName?: string;
}
