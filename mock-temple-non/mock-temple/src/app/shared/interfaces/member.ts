export interface Member {
  id?: Number;
  username?: String;
  password?: String;
  fname: String;
  lname: String;
  address?: String;
  tel?: String;
  emergencyTel?: String;
  email?: String;
  img?: String;
  roleId?: Number;
  roleName?: String;
  titleId?: Number;
  titleDisplay?: String;
  titleName?: String;
  genderId?: Number;
  genderName?: String;
  memberJob?: String;
  memberTransportation?: String;
  memberCoursePassed?: String;
  membermemberExpected?: String;
  memberExpPassed?: String;
  memberNote?: String;
}
