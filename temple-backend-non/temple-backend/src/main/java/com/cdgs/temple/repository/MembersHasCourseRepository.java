package com.cdgs.temple.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdgs.temple.entity.MembersHasCourseEntity;

@Repository
@Transactional(readOnly = true)
public interface MembersHasCourseRepository extends CrudRepository<MembersHasCourseEntity, Long> {
	
	@Query( value = "select COUNT(*) pass_courses from members_has_courses"
			+ " Where members_has_courses.member_id = :id and members_has_courses.status = 1"
			,nativeQuery = true)
    Long CountForPassCourse(@Param("id") Long id);
	
	
	@Modifying
	@Transactional
	@Query ( value = "UPDATE members_has_courses mhc" + 
			" INNER JOIN courses_teacher ct on mhc.course_id = ct.course_id" + 
			" SET status = :status" + 
			" WHERE ct.member_id = :monkId" + 
			" and mhc.members_has_course_id = :mchId" 
			,nativeQuery = true)
	void updateStatusMember(
			@Param("status") char status, 
			@Param("monkId") Long monkId,
			@Param("mchId") Long mchId
			);
	
	List<MembersHasCourseEntity> findAll();

	List<MembersHasCourseEntity> findAllByMemberIdAndStatus(Long memberId,char status);
	List<MembersHasCourseEntity> findAllByCourseId(Long courseId);
}
