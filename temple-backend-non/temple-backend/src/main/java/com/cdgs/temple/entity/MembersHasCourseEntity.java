package com.cdgs.temple.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "members_has_courses")
@Embeddable
public class MembersHasCourseEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5345044781843738399L;

    @Id
    @Column(name = "members_has_course_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long membersHasCourseId;

    @Column(name = "member_id")
    private long memberId;

    @Column(name = "course_id")
    private long courseId;

    @Column(name = "status")
    private char status;

    @Column(name = "register_date")
    @CreationTimestamp
    private LocalDateTime registerDate;
    

    @OneToOne
    @JoinColumn(name = "course_id", insertable = false, updatable = false)
    private CourseEntity course;

    public long getMembersHasCourseId() {
        return membersHasCourseId;
    }

    public void setMembersHasCourseId(long membersHasCourseId) {
        this.membersHasCourseId = membersHasCourseId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public LocalDateTime getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDateTime registerDate) {
        this.registerDate = registerDate;
    }

    public CourseEntity getCourse() {
        return course;
    }

    public void setCourse(CourseEntity course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "MembersHasCourseEntity [membersHasCourseId=" + membersHasCourseId + ", memberId=" + memberId
                + ", courseId=" + courseId + ", status=" + status + ", registerDate=" + registerDate + "]";
    }

}
