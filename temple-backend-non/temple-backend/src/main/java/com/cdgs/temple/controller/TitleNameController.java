package com.cdgs.temple.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cdgs.temple.dto.MapDto;
import com.cdgs.temple.dto.TitleNameDto;
import com.cdgs.temple.service.TitleNameService;
import com.cdgs.temple.util.ResponseDto;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1/titlenames")

public class TitleNameController {
	
	private static final Logger log = LoggerFactory.getLogger(TitleNameController.class);
	
	@Autowired
	TitleNameService titleNameService;

	@GetMapping(path = "")
	public ResponseEntity<ResponseDto<TitleNameDto>> getTitleNames() {
		List<TitleNameDto> dto = new ArrayList<TitleNameDto>();
		ResponseDto<TitleNameDto> res = new ResponseDto<TitleNameDto>();
		try {
			dto = titleNameService.getTitleNames();
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setData(dto);
			res.setCode(200);
		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<ResponseDto<TitleNameDto>>(res, HttpStatus.OK);

	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<ResponseDto<TitleNameDto>> putTitleName(@PathVariable("id") Long id) {
		List<TitleNameDto> dto = new ArrayList<TitleNameDto>();
		ResponseDto<TitleNameDto> res = new ResponseDto<TitleNameDto>();
		TitleNameDto titleName = new TitleNameDto();
		try {
			titleName = titleNameService.getTitleName(id);
			if(titleName != null) {
				dto.add(titleName);
			}
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setData(dto);
			res.setCode(200);
		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<ResponseDto<TitleNameDto>>(res, HttpStatus.OK);

	}
	
	@PostMapping(path = "")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<ResponseDto<TitleNameDto>> postTitleName(@Valid @RequestBody TitleNameDto body) {
		List<TitleNameDto> dto = new ArrayList<TitleNameDto>();
		ResponseDto<TitleNameDto> res = new ResponseDto<TitleNameDto>();
		TitleNameDto titleName = new TitleNameDto();
		try {
			titleName = titleNameService.createTitleName(body);
			if(titleName != null) {
				dto.add(titleName);
			}
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setData(dto);
			res.setCode(200);
		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<ResponseDto<TitleNameDto>>(res, HttpStatus.OK);

	}
	
	@PutMapping(path = "/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<ResponseDto<TitleNameDto>> getTitleName(@PathVariable("id") Long id, @Valid @RequestBody TitleNameDto body) {
		log.error(body.toString());
		List<TitleNameDto> dto = new ArrayList<TitleNameDto>();
		ResponseDto<TitleNameDto> res = new ResponseDto<TitleNameDto>();
		TitleNameDto titleName = new TitleNameDto();
		body.toString();
		try {
			titleName = titleNameService.updateTitleName(id,body);
			if(titleName != null) {
				dto.add(titleName);
			}
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setData(dto);
			res.setCode(200);
		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<ResponseDto<TitleNameDto>>(res, HttpStatus.OK);

	}
	
	@DeleteMapping(path = "/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<ResponseDto<TitleNameDto>> deleteTitleName(@PathVariable("id") Long id) {
		ResponseDto<TitleNameDto> res = new ResponseDto<TitleNameDto>();
		try {
			titleNameService.deleteTitleName(id);
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setCode(200);
		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<ResponseDto<TitleNameDto>>(res, HttpStatus.OK);

	}
	
	
	
	

}
