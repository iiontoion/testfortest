package com.cdgs.temple.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.cdgs.temple.entity.MembersHasCourseEntity;
import com.cdgs.temple.entity.TempSpecialApproveEntity;
import com.cdgs.temple.repository.MembersHasCourseRepository;
import com.cdgs.temple.repository.TempSpecialApproveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cdgs.temple.dto.SpecialApproveDto;
import com.cdgs.temple.entity.SpecialApproveEntity;
import com.cdgs.temple.repository.SpecialApproveRepository;
import com.cdgs.temple.service.SpecialApproveService;

@Service
public class SpecialApproveServiceImpl implements SpecialApproveService{

	private SpecialApproveRepository specialApproveRepository;
	private TempSpecialApproveRepository tempSpecialApproveRepository;
	private MembersHasCourseRepository membersHasCourseRepository;
	@Autowired
	public SpecialApproveServiceImpl (
			SpecialApproveRepository specialApproveRepository,
			TempSpecialApproveRepository tempSpecialApproveRepository,
			MembersHasCourseRepository membersHasCourseRepository
	) {
		this.specialApproveRepository = specialApproveRepository;
		this.tempSpecialApproveRepository = tempSpecialApproveRepository;
		this.membersHasCourseRepository = membersHasCourseRepository;
	}

	@Override
	public List<SpecialApproveDto> getAll(Long memberId, Long courseId) {
		return mapTempEntityListToDto(tempSpecialApproveRepository.getAll(memberId, courseId));
	}

	@Override
	public SpecialApproveDto getById(Long memberId, Long id) {
		return mapEntityToDto(specialApproveRepository.findBySpecialApproveId(id));
	}

	@Override
	public SpecialApproveDto create(SpecialApproveDto body) {

		SpecialApproveEntity entity = mapDtoToEntity(body);

		try {
			return mapEntityToDto(specialApproveRepository.save(entity));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public SpecialApproveDto delete(Long courseId, Long memberId) {
		SpecialApproveEntity data = specialApproveRepository.findByCourseIdAndMemberIdAndStatus(courseId, memberId, "2");
		if(data != null) {
			data.setStatus("3");
			data.setLastUpdate(LocalDateTime.now());
			return mapEntityToDto(specialApproveRepository.save(data));
		}
		return null;
	}

	@Override
	public SpecialApproveDto update(SpecialApproveDto body, Long memberId) {
		SpecialApproveEntity data = specialApproveRepository.fetchBySaIdAndMemberId(body.getSpecialApproveId(), memberId);
		if(data != null && !body.getStatus().equals("2") && !body.getStatus().equals("3")) {
			data.setStatus(body.getStatus());
			data.setLastUpdate(LocalDateTime.now());
			return mapEntityToDto(specialApproveRepository.save(data));
		}
		return null;
	}

	@Override
	public boolean approve(SpecialApproveDto data) {
		MembersHasCourseEntity mhc = new MembersHasCourseEntity();
		mhc.setMemberId(data.getMemberId());
		mhc.setCourseId(data.getCourseId());
		mhc.setStatus('2');
		if (membersHasCourseRepository.save(mhc) != null){
			return true;
		}
		return false;
	}

	private List<SpecialApproveDto> mapEntityListToDto(List<SpecialApproveEntity> entities){
		List<SpecialApproveDto> dto = new ArrayList<>();
		for (SpecialApproveEntity entity : entities) {
			dto.add(mapEntityToDto(entity));
		}
		return dto;
	}

	private List<SpecialApproveDto> mapTempEntityListToDto(List<TempSpecialApproveEntity> entities) {
		List<SpecialApproveDto> dto = new ArrayList<>();
		for (TempSpecialApproveEntity entity : entities) {
			dto.add(mapTempEntityToDto(entity));
		}
		return dto;
	}

	private SpecialApproveDto mapEntityToDto(SpecialApproveEntity entity) {
		SpecialApproveDto dto = new SpecialApproveDto();
		dto.setSpecialApproveId(entity.getSpecialApproveId());
		dto.setCourseId(entity.getCourseId());
		dto.setCreateDate(entity.getCreateDate());
		dto.setDetail(entity.getDetail());
		dto.setLastUpdate(entity.getLastUpdate());
		dto.setMemberId(entity.getMemberId());
		dto.setStatus(entity.getStatus());

		return dto;
	}

	private SpecialApproveDto mapTempEntityToDto(TempSpecialApproveEntity entity) {
		SpecialApproveDto dto = new SpecialApproveDto();
		dto.setSpecialApproveId(entity.getSpecialApproveId());
		dto.setMemberId(entity.getMemberId());
		dto.setDisplayName(entity.getDisplayName());

		return dto;
	}

	private SpecialApproveEntity mapDtoToEntity(SpecialApproveDto dto) {
		SpecialApproveEntity entity = new SpecialApproveEntity();
		entity.setCourseId(dto.getCourseId());
		entity.setCreateDate(dto.getCreateDate());
		entity.setDetail(dto.getDetail());
		entity.setLastUpdate(dto.getLastUpdate());
		entity.setMemberId(dto.getMemberId());
		entity.setStatus(dto.getStatus());
		return entity;
	}


}
