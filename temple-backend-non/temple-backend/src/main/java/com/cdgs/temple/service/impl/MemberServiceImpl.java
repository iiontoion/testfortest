package com.cdgs.temple.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.cdgs.temple.security.JwtUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.cdgs.temple.dto.MemberDto;
import com.cdgs.temple.entity.MemberEntity;
import com.cdgs.temple.repository.MemberRepository;
import com.cdgs.temple.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService {

	private static final Logger log = LoggerFactory.getLogger(MemberServiceImpl.class);
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	private final MemberRepository memberRepository;

	@Autowired
	public MemberServiceImpl(MemberRepository memberRepository) {
		this.memberRepository = memberRepository;
	}

	@Override
	public List<MemberDto> getMembers() {
		List<MemberEntity> memberEntities = new ArrayList<>();
		try {
			memberEntities = memberRepository.findAll();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return mapListEntityToDto(memberEntities);
	}

	@Override
	public List<MemberDto> getTeacher() {
		List<MemberEntity> memberEntities = new ArrayList<>();
		try {
			memberEntities = memberRepository.findAllByRole_RoleName("monk");
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return mapListEntityToDto(memberEntities);
	}

	@Override
	public MemberDto getMember(Long id) {

		MemberEntity entity = memberRepository.findById(id).get();
		return mapEntityToDto(entity);
	}

	@Override
	public MemberDto createMember(MemberDto member) {
		
		MemberEntity memberData = mapDtoToEntity(member);
		MemberEntity entity ;
		memberData.setMemberRoleId((long) 3);
		memberData.setMemberPassword(bCryptPasswordEncoder.encode(memberData.getMemberPassword()));
		entity = memberRepository.save(memberData);
		//return mapEntityToDtoFnSave(entity);
		return mapEntityToDto(entity);

	}

	@Override
	public MemberDto updateMember(Long id, MemberDto member) {	
		MemberEntity memberConvert = mapDtoToEntity(member);
		MemberEntity entity = new MemberEntity();
		
		System.out.println(member.toString());

		Optional<MemberEntity> memberEntity = memberRepository.findById(id);
		if(!memberEntity.isPresent()) {
			return mapEntityToDto(memberEntity.get());
		}

		memberConvert.setMemberId(id);
		memberConvert.setMemberUsername(memberEntity.get().getMemberUsername());
		memberConvert.setMemberPassword(memberEntity.get().getMemberPassword());
		
		memberConvert.setMemberRoleId(memberEntity.get().getRole().getRoleId());

		entity = memberRepository.save(memberConvert);
		return mapEntityToDto(entity);

	}
	
	@Override
	public MemberDto createMemberByAdmin(MemberDto member) {
		MemberEntity memberData = mapDtoToEntity(member);
		MemberEntity entity ;
		//memberData.setMemberRoleId((long) member.getRoleId());
		memberData.setMemberPassword(bCryptPasswordEncoder.encode(memberData.getMemberPassword()));
		entity = memberRepository.save(memberData);
		return mapEntityToDto(entity);
	}
	
	@Override
	public MemberDto updateMemberByAdmin(Long id, MemberDto member) {
		MemberEntity memberConvert = mapDtoToEntity(member);
		MemberEntity entity ;

		Optional<MemberEntity> memberEntity = memberRepository.findById(id);
		if(!memberEntity.isPresent()) {
			return mapEntityToDto(memberEntity.get());
		}

		memberConvert.setMemberId(id);
		memberConvert.setMemberUsername(memberEntity.get().getMemberUsername());
		memberConvert.setMemberPassword(memberEntity.get().getMemberPassword());
		entity = memberRepository.save(memberConvert);
		return mapEntityToDto(entity);
		
	}

	@Override
	public MemberDto getCurrentMember() {
		JwtUser member = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return mapEntityToDto(member.getMember());
	}

	private MemberEntity mapDtoToEntity(MemberDto member) {
		MemberEntity entity = new MemberEntity();
		entity.setMemberGenderId(member.getGenderId());
		entity.setMemberTitleId(member.getTitleId());
		entity.setMemberRoleId(member.getRoleId());
		entity.setMemberId(member.getId());		
		entity.setMemberUsername(member.getUsername());
		entity.setMemberPassword(member.getPassword());
		entity.setMemberFname(member.getFname());
		entity.setMemberLname(member.getLname());
		entity.setMemberAddress(member.getAddress());
		entity.setMemberTel(member.getTel());   
		entity.setMemberEmergencyTel(member.getEmergencyTel());
		entity.setMemberEmail(member.getEmail());
		entity.setMemberImg(member.getImg());
		entity.setMemberRegisterDate(member.getRegisterDate());
		entity.setMemberLastUpdate(member.getLastUpdate());
		entity.setMemberJob(member.getMemberJob());
		entity.setMemberTransportation(member.getMemberTransportation());
		entity.setMemberCoursePassed(member.getMemberCoursePassed());
		entity.setMemberExpPassed(member.getMemberExpPassed());
		entity.setMemberExpected(member.getMemberExpected());
		entity.setMemberNote(member.getMemberNote());
		
		return entity;
	}

	private List<MemberDto> mapListEntityToDto(List<MemberEntity> entities) {
		List<MemberDto> dtoList = new ArrayList<>();
		if (!entities.isEmpty()) {
			for (MemberEntity entity : entities) {
				dtoList.add(mapEntityToDto(entity));
			}
		}
		return dtoList;
	}

	private MemberDto mapEntityToDto(MemberEntity entity) {
		MemberDto dto = new MemberDto();
		if (entity != null) {
			dto.setId(entity.getMemberId());
			dto.setUsername(entity.getMemberUsername());
			dto.setFname(entity.getMemberFname());
			dto.setLname(entity.getMemberLname());
			dto.setAddress(entity.getMemberAddress());
			dto.setTel(entity.getMemberTel());
			dto.setEmergencyTel(entity.getMemberEmergencyTel());
			dto.setEmail(entity.getMemberEmail());
			dto.setImg(entity.getMemberImg());
			dto.setRegisterDate(entity.getMemberRegisterDate());
			dto.setLastUpdate(entity.getMemberLastUpdate());
			dto.setMemberJob(entity.getMemberJob());
			dto.setMemberTransportation(entity.getMemberTransportation());
			dto.setMemberExpPassed(entity.getMemberExpPassed());
			dto.setMemberExpected(entity.getMemberExpected());
			dto.setMemberNote(entity.getMemberNote());
			dto.setMemberCoursePassed(entity.getMemberCoursePassed());
			
			
			dto.setGenderId(entity.getMemberGenderId());
			if(entity.getGender() != null)
				dto.setGenderName(entity.getGender().getGenderName());
			
			dto.setTitleId(entity.getMemberTitleId());
			if(entity.getTitleName() != null) {
				dto.setTitleDisplay(entity.getTitleName().getTitleDisplay());
				dto.setTitleName(entity.getTitleName().getTitleName());
			}
			
			dto.setRoleId(entity.getMemberRoleId());
			if(entity.getRole() != null)
				dto.setRoleName(entity.getRole().getRoleName());
			
		}
		return dto;
	}








	
}
