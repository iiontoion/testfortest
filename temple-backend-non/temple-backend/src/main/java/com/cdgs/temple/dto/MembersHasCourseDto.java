package com.cdgs.temple.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

public class MembersHasCourseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8061410967659591652L;
	
	private long membersHasCourseId;
	private long memberId;
	private long courseId;
	private char status;
	private LocalDateTime registerDate;
	
	public long getMembersHasCourseId() {
		return membersHasCourseId;
	}
	public void setMembersHasCourseId(long membersHasCourseId) {
		this.membersHasCourseId = membersHasCourseId;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public LocalDateTime getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(LocalDateTime registerDate) {
		this.registerDate = registerDate;
	}
}
