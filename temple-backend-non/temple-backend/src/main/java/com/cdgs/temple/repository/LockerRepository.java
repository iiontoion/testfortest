package com.cdgs.temple.repository;

import java.util.List;
import com.cdgs.temple.entity.LockerEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LockerRepository extends CrudRepository<LockerEntity, Long> {
	
	List<LockerEntity> findAllByEnableIsTrue();
	List<LockerEntity> findAllByEnableIsTrueAndIsActive(char isActive);

	LockerEntity findAllByLocationIdAndLockerNumber(long locationId, String lockerNumber);

	LockerEntity findByLocationIdAndLockerNumberAndEnableIsTrue(long locationId, String lockerNumber);

	//Boolean findByLocationIdAndLockerNumber(long locationId, String lockerNumber);

	int countByLocationIdAndLockerNumber(long locationId, String lockerNumber);

}
