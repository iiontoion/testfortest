package com.cdgs.temple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TempleApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TempleApiApplication.class, args);
	}

}
