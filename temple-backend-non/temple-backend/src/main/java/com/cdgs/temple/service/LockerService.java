package com.cdgs.temple.service;

import java.util.List;

import com.cdgs.temple.dto.LockerDto;

public interface LockerService {
    List<LockerDto> getAll();

    List<LockerDto> getAllByEnableIsTrueAndIsNotActive();

    LockerDto getLockerById(long locationId, String lockerNumber);

    LockerDto create(LockerDto locker);

    LockerDto update(long locationId, String lockerNumber, LockerDto locker);

    LockerDto delete(long locationId, String lockerNumber);

}
