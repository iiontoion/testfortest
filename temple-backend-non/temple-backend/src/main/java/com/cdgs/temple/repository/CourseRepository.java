package com.cdgs.temple.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cdgs.temple.entity.CourseEntity;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface CourseRepository extends CrudRepository<CourseEntity, Long> {
	List<CourseEntity> findAll();
	
	Integer countAllByEnableIsTrue();
	
	@Query(value = "SELECT COUNT(mhc.course_id) FROM members_has_courses mhc WHERE mhc.course_id = :courseId AND mhc.member_id = :memberId AND (mhc.status = '1' OR mhc.status = '2')", nativeQuery = true)
	Integer findCoursesUserRegister(@Param("memberId") Long memberId, @Param("courseId") Long courseId);

	@Query(value = "SELECT c.*FROM courses c INNER JOIN courses_teacher ct ON c.course_id=ct.course_id INNER JOIN special_approve sa ON c.course_id=sa.course_id WHERE 1=1 AND ct.member_id=:memberId AND sa.STATUS='2' AND (CASE WHEN :query <> '' THEN (c.course_name LIKE CONCAT('%',:query,'%') OR c.course_detail LIKE CONCAT('%',:query,'%') OR c.course_condition_min LIKE CONCAT('%',:query,'%')) ELSE 1 END) GROUP BY c.course_id ORDER BY c.course_id LIMIT :offset, :limit", nativeQuery = true)
	List<CourseEntity> fetchCoursesTeacherApproval(@Param("memberId") Long memberId, @Param("offset") int offset, @Param("limit") int limit, @Param("query") String query);

	@Query(value = "SELECT COUNT(*) total_record FROM (" +
			"SELECT c.course_id FROM courses c INNER JOIN courses_teacher ct ON c.course_id=ct.course_id INNER JOIN special_approve sa ON c.course_id=sa.course_id WHERE 1=1 AND ct.member_id=:memberId AND sa.STATUS='2' GROUP BY c.course_id) t1", nativeQuery = true)
	Integer countCoursesTeacherApprovalAll(@Param("memberId") Long memberId);
	
	@Query(value = "SELECT * FROM courses c  WHERE 1=1 AND c.enable = 1  AND (CASE WHEN :query <> '' THEN (c.course_name LIKE CONCAT('%',:query,'%') OR c.course_detail LIKE CONCAT('%',:query,'%') OR c.course_condition_min LIKE CONCAT('%',:query,'%')) ELSE 1 END) GROUP BY c.course_id ORDER BY c.course_id LIMIT :offset, :limit", nativeQuery = true)
	List<CourseEntity> selectAll(@Param("offset") int offset, @Param("limit") int limit, @Param("query") String query);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE courses as c   " + 
			"set c.enable = 0  " + 
			"WHERE c.course_id = :courseId",nativeQuery = true)
	void deleteCourse(@Param("courseId") Long id);
}
