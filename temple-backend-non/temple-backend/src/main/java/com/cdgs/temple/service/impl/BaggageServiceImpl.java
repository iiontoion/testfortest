package com.cdgs.temple.service.impl;

import com.cdgs.temple.dto.BaggageDto;
import com.cdgs.temple.entity.BaggageEntity;
import com.cdgs.temple.entity.LocationEntity;
import com.cdgs.temple.entity.LockerEntity;
import com.cdgs.temple.repository.BaggageRepository;
import com.cdgs.temple.repository.LocationRepository;
import com.cdgs.temple.repository.LockerRepository;
import com.cdgs.temple.service.BaggageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

@Service
public class BaggageServiceImpl implements BaggageService {

    private BaggageRepository baggageRepository;

    private LockerRepository lockerRepository;

    @Autowired(required = false)
    public BaggageServiceImpl(BaggageRepository baggageRepository, LockerRepository lockerRepository) {
        this.baggageRepository = baggageRepository;
        this.lockerRepository = lockerRepository;
    }

    @Override
    public List<BaggageDto> getAll() {
        List<BaggageEntity> entities = baggageRepository.findAll();
        return mapListEntityToDto(entities);
    }

    @Override
    public List<BaggageDto> getByMemberId(Long memberId) {
        List<BaggageEntity> entities = baggageRepository.findAllByMemberId(memberId);
        return mapListEntityToDto(entities);
    }

    @Override
    public BaggageDto create(BaggageDto body) {
        LockerEntity lockerEntity = lockerRepository.findByLocationIdAndLockerNumberAndEnableIsTrue(body.getLocationId(), body.getNumber());
        char isActive = lockerEntity.getIsActive();
        //check locker isn't using;
        if (isActive == '0') {
        	body.setStatus('0');
            BaggageEntity entity = baggageRepository.save(mapDtoToEntity(body));
            //change status is_active = using ;
            lockerEntity.setIsActive('1');
            lockerRepository.save(lockerEntity);
            return mapEntityToDto(entity);
        } else {
            throw new RuntimeErrorException(null, "Can't not update. Locker is active");
        }


    }

    @Override
    public BaggageDto delete(Long baggageId) {
        BaggageEntity entity = baggageRepository.findAllByBaggageId(baggageId);
        if (!(entity == null)) {
            entity.setBaggageLastUpdate(LocalDateTime.now());
            entity.setStatus('3');
            return mapEntityToDto(baggageRepository.save(entity));
        }
        return null;
    }

    @Override
    public BaggageDto update(Long baggageId, BaggageDto body) {
        BaggageDto baggageDto;
        LocationEntity location = new LocationEntity();
        //edit status
        BaggageEntity entity = baggageRepository.findById(baggageId).get();
        System.out.println("entitylockerLocationId = "+entity.getLockerLocationId());
        System.out.println("entitylockerNumber = "+entity.getLockerNumber());



        entity.setBaggageId(baggageId);
        System.out.println("bagId = "+baggageId);
        entity.setMemberId(body.getMemberId());
        System.out.println("memberId = "+body.getMemberId());
        entity.setLockerNumber(entity.getLockerNumber());
        System.out.println("lockerNumber = "+entity.getLockerNumber());

        //location
        location.setLocationId(entity.getLockerLocationId());
        System.out.println("lockerLocationId = "+location.getLocationId());

        entity.setLocation(location);
        
        entity.setStatus(body.getStatus());
        System.out.println("status = "+body.getStatus());

        LockerEntity lockerEntity = lockerRepository.findByLocationIdAndLockerNumberAndEnableIsTrue(entity.getLockerLocationId(), entity.getLockerNumber());
        //update
        baggageDto = mapEntityToDto(baggageRepository.save(entity));
        //change status is_active != using ;
        lockerEntity.setIsActive(body.getStatus());
        lockerRepository.save(lockerEntity);
        return baggageDto;
    }


    private BaggageEntity mapDtoToEntity(BaggageDto dto) {
        BaggageEntity entity = new BaggageEntity();
        entity.setStatus(dto.getStatus());
        entity.setLockerLocationId(dto.getLocationId());
        entity.setMemberId(dto.getMemberId());
        entity.setLockerNumber(dto.getNumber());
        return entity;
    }


    private List<BaggageDto> mapListEntityToDto(List<BaggageEntity> entities) {
        List<BaggageDto> dtoList = new ArrayList<>();
        if (!entities.isEmpty()) {
            for (BaggageEntity entity : entities) {
                dtoList.add(mapEntityToDto(entity));
            }
        }
        return dtoList;
    }


    private BaggageDto mapEntityToDto(BaggageEntity entity) {
        BaggageDto dto = new BaggageDto();
        if (entity != null) {
            dto.setBaggageId(entity.getBaggageId());
            dto.setCreateDate(entity.getBaggageCreateDate());
            dto.setLastUpdate(entity.getBaggageLastUpdate());
            dto.setStatus(entity.getStatus());
            dto.setMemberId(entity.getMemberId());
            dto.setLocationId(entity.getLockerLocationId());
            dto.setNumber(entity.getLockerNumber());

            if (entity.getMember() == null) {
                dto.setMemberName(null);
            } else {
                String name = entity.getMember().getTitleName().getTitleDisplay() + "" + entity.getMember().getMemberFname() + " " + entity.getMember().getMemberLname();
                dto.setMemberName(name);
            }

            if (entity.getLocation() == null) {
                dto.setLocationName(null);
            } else {
                dto.setLocationName(entity.getLocation().getLocationName());
            }

        }
        return dto;
    }
}
