package com.cdgs.temple.service;

import java.util.List;

import com.cdgs.temple.dto.CourseDto;
import com.cdgs.temple.dto.MembersHasCourseDto;

public interface CourseService {
	List<CourseDto> getCoursesUser(Long memberId,String query,int offset,int limit);
	List<CourseDto> getCourses(String query,int offset,int limit);
	CourseDto getCourse(Long id);
	CourseDto createCourse(CourseDto body);
    Boolean deleteCourse(long id) ;
	CourseDto updateCourse(Long id,CourseDto body);
	MembersHasCourseDto assignCourse(MembersHasCourseDto body);
	Integer getCoursesUserRegister(Long memberId, Long courseId);
	CourseDto getCourseUser(Long memberId, Long courseId);
	
	List<CourseDto> getHistory(Long memberId);

    List<CourseDto> TeacherGetCoursesApproval(Long memberId, int offset, int limit, String query);

	Integer CountTeacherCoursesApproval(Long memberId);
	Integer countCourses();
}
