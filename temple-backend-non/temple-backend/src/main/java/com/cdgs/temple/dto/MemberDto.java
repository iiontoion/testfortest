package com.cdgs.temple.dto;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;


public class MemberDto implements Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4601867391972524109L;
	private Long id;
	private String username;
	private String password;
	private String fname;
	private String lname;
	private String address;
	private String tel;
	private String emergencyTel;
	private String email;
	private String img;
	private LocalDateTime registerDate;
	private LocalDateTime lastUpdate;
	
	private Long roleId;
	private String roleName;
	
	private Long titleId;
	private String titleDisplay;
	private String titleName;
	
	private Long genderId;
	private String genderName;

	private String memberJob;

	private String memberTransportation;
	private String memberCoursePassed;
	private String memberExpPassed;
	private String memberExpected;
	private String memberNote;
	

	public String getMemberExpPassed() {
		return this.memberExpPassed;
	}

	public void setMemberExpPassed(String memberExpPassed) {
		this.memberExpPassed = memberExpPassed;
	}

	public String getMemberExpected() {
		return this.memberExpected;
	}

	public void setMemberExpected(String memberExpected) {
		this.memberExpected = memberExpected;
	}

	public String getMemberNote() {
		return this.memberNote;
	}

	public void setMemberNote(String memberNote) {
		this.memberNote = memberNote;
	}
	
	public String getMemberCoursePassed() {
		return memberCoursePassed;
	}
	public void setMemberCoursePassed(String memberCoursePassed) {
		this.memberCoursePassed = memberCoursePassed;
	}
	public String getMemberTransportation() {
		return memberTransportation;
	}
	public void setMemberTransportation(String memberTransportation) {
		this.memberTransportation = memberTransportation;
	}
	public String getMemberJob() {
		return memberJob;
	}
	public void setMemberJob(String memberJob) {
		this.memberJob = memberJob;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getGenderName() {
		return genderName;
	}
	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	public String getTitleDisplay() {
		return titleDisplay;
	}
	public void setTitleDisplay(String titleDisplay) {
		this.titleDisplay = titleDisplay;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmergencyTel() {
		return emergencyTel;
	}
	public void setEmergencyTel(String emergencyTel) {
		this.emergencyTel = emergencyTel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public LocalDateTime getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(LocalDateTime registerDate) {
		this.registerDate = registerDate;
	}
	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getTitleId() {
		return titleId;
	}
	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}
	public Long getGenderId() {
		return genderId;
	}
	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	
	
	
	
	
	
}
