package com.cdgs.temple.service;

import java.util.List;

import com.cdgs.temple.dto.SpecialApproveDto;

public interface SpecialApproveService {
    List<SpecialApproveDto> getAll(Long memberId, Long courseId);

    SpecialApproveDto getById(Long memberId, Long id);

    SpecialApproveDto create(SpecialApproveDto body);

    SpecialApproveDto delete(Long courseId, Long memberId);

    SpecialApproveDto update(SpecialApproveDto body, Long id);

    boolean approve(SpecialApproveDto data);
}
