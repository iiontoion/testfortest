package com.cdgs.temple.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import com.cdgs.temple.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.cdgs.temple.entity.CourseEntity;
import com.cdgs.temple.service.CourseScheduleService;
import com.cdgs.temple.service.CourseService;
import com.cdgs.temple.service.CourseTeacherService;
import com.cdgs.temple.service.MemberService;
import com.cdgs.temple.service.MembersHasCourseService;
import com.cdgs.temple.service.SpecialApproveService;
import com.cdgs.temple.util.ResponseDto;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1/courses")
public class CourseController {

    private MemberService memberService;
    private CourseService courseService;
    private MembersHasCourseService membersHasCourseService;
    private CourseScheduleService courseScheduleService;
    private CourseTeacherService courseTeacherservice;
    private SpecialApproveService specialApproveService;

    @Autowired
    public CourseController(CourseService courseService, MemberService memberService,
            MembersHasCourseService membersHasCourseService, CourseScheduleService courseScheduleService,
            CourseTeacherService courseTeacherService, SpecialApproveService specialApproveService) {
        this.specialApproveService = specialApproveService;
        this.courseService = courseService;
        this.memberService = memberService;
        this.membersHasCourseService = membersHasCourseService;
        this.courseScheduleService = courseScheduleService;
        this.courseTeacherservice = courseTeacherService;
    }

    // @GetMapping(path = "")
    // @PreAuthorize("hasRole('admin') or hasRole('monk') or hasRole('user')")
    // public ResponseEntity<List<CourseEntity>> getAllUsers() {
    // List<CourseEntity> user = courseRepository.findAll();
    // return new ResponseEntity<List<CourseEntity>>(user, HttpStatus.OK);
    // }

    @GetMapping(path = "/allmembers/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('monk') or hasRole('user')")
    public ResponseEntity<ResponseDto<MembersHasCourseDto>> getAllUsers(@PathVariable(value = "id") Long courseId) {
        // @GetMapping(path = "/allmembers")
        // @PreAuthorize("hasRole('admin') or hasRole('monk') or hasRole('user')")
        // public ResponseEntity<ResponseDto<MembersHasCourseDto>>
        // getAllUsers(@RequestParam("courseId") Long courseId) {
        ResponseDto<MembersHasCourseDto> res = new ResponseDto<>();
        try {
            List<MembersHasCourseDto> memberHasCourse = membersHasCourseService.getMembersByCourse(courseId);
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            
            res.setData(memberHasCourse);
            res.setCode(200);
        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(path = "")
    @PreAuthorize("hasRole('admin') or hasRole('monk') or hasRole('user')")
    public ResponseEntity<ResponseDto<CourseDto>> GetCourses(@RequestParam("offset") int offset,
            @RequestParam("limit") int limit, @RequestParam("query") String query) {
        ResponseDto<CourseDto> res = new ResponseDto<>();
        List<CourseDto> dto;
        MemberDto member = memberService.getCurrentMember();

        try {
            if (member.getRoleName().equals("admin") || member.getRoleName().equals("monk")) {
                dto = courseService.getCourses(query, offset, limit);
            } else {
                dto = courseService.getCoursesUser(member.getId(), query, offset, limit);
            }
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(dto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(value = "/count")
    @PreAuthorize("hasRole('admin') or hasRole('monk') or hasRole('user')")
    public ResponseEntity<ResponseDto<ResponseCountDto>> countCourses() {
        ResponseDto<ResponseCountDto> res = new ResponseDto<>();
        List<ResponseCountDto> listDto = new ArrayList<>();
        ResponseCountDto dto = new ResponseCountDto();
        int count;
        MemberDto member = memberService.getCurrentMember();
        try {
            count = courseService.countCourses();
            dto.setTotalRecord(count);
            listDto.add(dto);
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(listDto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(value = "/approve")
    @PreAuthorize("hasRole('monk')")
    public ResponseEntity<ResponseDto<CourseDto>> TeacherGetCoursesApproval(@RequestParam("offset") int offset,
            @RequestParam("limit") int limit, @RequestParam("query") String query) {
        ResponseDto<CourseDto> res = new ResponseDto<>();
        List<CourseDto> dto;
        MemberDto member = memberService.getCurrentMember();

        try {
            System.out.println(offset);
            System.out.println(limit);
            dto = courseService.TeacherGetCoursesApproval(member.getId(), offset, limit, query);
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(dto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(value = "/approve/count")
    @PreAuthorize("hasRole('monk')")
    public ResponseEntity<ResponseDto<ResponseCountDto>> CountTeacherCoursesApproval() {
        ResponseDto<ResponseCountDto> res = new ResponseDto<>();
        List<ResponseCountDto> listDto = new ArrayList<>();
        ResponseCountDto dto = new ResponseCountDto();
        int count;
        MemberDto member = memberService.getCurrentMember();
        try {
            count = courseService.CountTeacherCoursesApproval(member.getId());
            dto.setTotalRecord(count);
            listDto.add(dto);
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(listDto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('monk') or hasRole('user')")
    public ResponseEntity<ResponseDto<CourseDto>> getCoursesById(@PathVariable("id") Long id) {
        ResponseDto<CourseDto> res = new ResponseDto<>();
        List<CourseDto> dto = new ArrayList<>();
        MemberDto member = memberService.getCurrentMember();

        try {
            if (member.getRoleName().equals("user")) {
                dto.add(courseService.getCourseUser(member.getId(), id));
                System.out.println(dto);

            } else {
                dto.add(courseService.getCourse(id));
                System.out.println(id);

                System.out.println(member.getRoleName());


            }

            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(dto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping(path = "")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<ResponseDto<CourseDto>> createCourse(@Valid @RequestBody CourseDto body) {
        ResponseDto<CourseDto> res = new ResponseDto<>();
        List<CourseDto> courses = new ArrayList<>();
        CourseDto course;
        CourseScheduleDto courseSchedule = new CourseScheduleDto();
        CourseTeacherDto courseTeacher = new CourseTeacherDto();
        try {
            course = courseService.createCourse(body);
            courseSchedule.setCourseId(course.getId());

            for (LocalDate date : body.getDate()) {
                System.out.println("***date = " + date);
                courseSchedule.setCourseScheduleDate(date);
                courseScheduleService.createCourseSchedule(courseSchedule);
            }

            courseTeacher.setCourseId(course.getId());
            for (Long tId : body.getTeacher()) {
                courseTeacher.setMemberId(tId);
                courseTeacher = courseTeacherservice.createCourseTeacher(courseTeacher);

            }

            courses.add(course);
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(courses);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping(path = "/{id}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<ResponseDto<CourseDto>> putCourse(@PathVariable("id") Long id,
            @Valid @RequestBody CourseDto body) {
        ResponseDto<CourseDto> res = new ResponseDto<>();
        List<CourseDto> courses = new ArrayList<>();
        CourseTeacherDto courseTeacher = new CourseTeacherDto();
        CourseDto course;
        try {
            System.out.println("print");
            course = courseService.updateCourse(id, body);
            System.out.println("body st = " + body.getStDate());
            System.out.println("body condition = " + body.getConditionMin());

            System.out.println("body end = " + body.getEndDate());
            System.out.println("course end = " + course.getStDate());
            System.out.println("course end = " + course.getEndDate());
            System.out.println(body);
            if (body != null) {
                courses.add(body);
            }
            courseTeacher.setCourseId(course.getId());
            for (Long tId : body.getTeacher()) {
                courseTeacher.setMemberId(tId);
                courseTeacher = courseTeacherservice.createCourseTeacher(courseTeacher);

            }
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(courses);
            res.setCode(200);
        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<ResponseDto<CourseEntity>> deleteCourse(@PathVariable("id") long id) {
        ResponseDto<CourseEntity> res = new ResponseDto<>();
        if (courseService.deleteCourse(id)) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setCode(204);
        } else {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping(path = "/register")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<ResponseDto<MembersHasCourseDto>> createMemberHasCourse(
            @Valid @RequestBody MembersHasCourseDto body) {
        ResponseDto<MembersHasCourseDto> res = new ResponseDto<>();
        List<MembersHasCourseDto> courses = new ArrayList<>();
        MembersHasCourseDto memberHasCourse;
        MemberDto member = memberService.getCurrentMember();

        try {
            if (courseService.getCoursesUserRegister(member.getId(), body.getCourseId()) == 0) {
                body.setMemberId(member.getId());
                body.setStatus('2');
                memberHasCourse = courseService.assignCourse(body);
                if (memberHasCourse == null)
                    throw new Exception("เงื่อนไขการสมัครไม่ถูกต้อง");
                courses.add(memberHasCourse);
                res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
                res.setData(courses);
            } else {
                res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
                res.setErrorMessage("Duplicate register");
                res.setCode(200);
            }
        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PostMapping(path = "/schedule")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<ResponseDto<CourseScheduleDto>> postCourseSchedule(
            @Valid @RequestBody CourseScheduleDto courseSchedule) {
        ResponseDto<CourseScheduleDto> res = new ResponseDto<>();
        List<CourseScheduleDto> courseSchedules = new ArrayList<>();
        CourseScheduleDto dto;
        try {
            dto = courseScheduleService.createCourseSchedule(courseSchedule);
            courseSchedules.add(dto);
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(courseSchedules);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(path = "/schedule")
    @PreAuthorize("hasRole('user')")
    public ResponseEntity<ResponseDto<CourseScheduleDto>> getCourseScheduleByUser() {
        ResponseDto<CourseScheduleDto> res = new ResponseDto<>();
        List<CourseScheduleDto> dto;
        MemberDto member = memberService.getCurrentMember();
        try {
            dto = courseScheduleService.getCourseScheduleByUser(member.getId());
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(dto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(path = "/teacher_schedule")
    @PreAuthorize("hasRole('monk')")
    public ResponseEntity<ResponseDto<CourseScheduleDto>> getCourseScheduleByMonk() {
        ResponseDto<CourseScheduleDto> res = new ResponseDto<>();
        List<CourseScheduleDto> dto;
        MemberDto member = memberService.getCurrentMember();
        try {
            dto = courseScheduleService.getCourseScheduleByMonk(member.getId());
            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(dto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping(path = "/deleteCourse/{id}")
    @PreAuthorize("hasRole('monk')")
    public ResponseEntity<ResponseDto<CourseDto>> deleteCourse(@PathVariable("id") Long id) {
        ResponseDto<CourseDto> res = new ResponseDto<>();
        try {
            if (courseService.deleteCourse(id))
                res.setResult("Success");
            else
                res.setResult("Fail");
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @GetMapping(value = "/history/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('monk') or hasRole('user')")
    public ResponseEntity<ResponseDto<CourseDto>> getHistoryCourseLearn(@PathVariable("id") Long id) {
        ResponseDto<CourseDto> res = new ResponseDto<>();
        List<CourseDto> dto = new ArrayList<>();
        MemberDto member = memberService.getCurrentMember();

        try {
            if (member.getRoleName().equals("user")) {
                // dto.add(courseService.getHistory(member.getId()));
                dto = courseService.getHistory(member.getId());
            } else {
                // dto.add(courseService.getHistory(id));
                dto = courseService.getHistory(id);
            }

            res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
            res.setData(dto);
            res.setCode(200);

        } catch (Exception e) {
            res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
            res.setErrorMessage(e.getMessage());
            res.setCode(200);
        }
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

}
