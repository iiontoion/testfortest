package com.cdgs.temple.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class SpecialApproveDto implements Serializable {


    private static final long serialVersionUID = -6856109137261338018L;
    
    private long specialApproveId;
    private List<Long> saId ;
    private long memberId;
    private long courseId;
    private String detail;
    private String status;
    private LocalDateTime createDate;
    private LocalDateTime lastUpdate;
    private String courseName;
    private String displayName;

    public long getSpecialApproveId() {
        return specialApproveId;
    }

    public void setSpecialApproveId(long specialApproveId) {
        this.specialApproveId = specialApproveId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

	public List<Long> getSaId() {
		return saId;
	}

	public void setSaId(List<Long> saId) {
		this.saId = saId;
	}
    
}
