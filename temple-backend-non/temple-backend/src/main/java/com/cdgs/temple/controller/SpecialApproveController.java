package com.cdgs.temple.controller;

import com.cdgs.temple.dto.MemberDto;
import com.cdgs.temple.dto.SpecialApproveDto;
import com.cdgs.temple.service.MemberService;
import com.cdgs.temple.service.SpecialApproveService;
import com.cdgs.temple.util.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1/approve")
public class SpecialApproveController {

	private SpecialApproveService specialApproveService;
	private MemberService memberService;

	@Autowired
	public SpecialApproveController(SpecialApproveService specialApproveService, MemberService memberService) {
		this.specialApproveService = specialApproveService;
		this.memberService = memberService;
	}

	@GetMapping(path = "/{courseId}")
	@PreAuthorize("hasRole('monk')")
	public ResponseEntity<ResponseDto<SpecialApproveDto>> GetAll(@PathVariable("courseId") Long courseId) {
		List<SpecialApproveDto> dto;
		ResponseDto<SpecialApproveDto> res = new ResponseDto<>();
		MemberDto member = memberService.getCurrentMember();
		try {
			dto = specialApproveService.getAll(member.getId(), courseId);
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setData(dto);
			res.setCode(200);
		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@PostMapping(path = "")
	@PreAuthorize("hasRole('user')")
	public ResponseEntity<ResponseDto<SpecialApproveDto>> Create(@Valid @RequestBody SpecialApproveDto body) {
		ResponseDto<SpecialApproveDto> res = new ResponseDto<>();
		List<SpecialApproveDto> specialApproves = new ArrayList<>();

		SpecialApproveDto dto;
		MemberDto member = memberService.getCurrentMember();
		body.setMemberId(member.getId());
		body.setStatus("2");

		try {
			dto = specialApproveService.create(body);
			specialApproves.add(dto);
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setData(specialApproves);
			res.setCode(200);

		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@PutMapping(path = "")
	@PreAuthorize("hasRole('monk')")
	public ResponseEntity<ResponseDto<SpecialApproveDto>> Update(@Valid @RequestBody SpecialApproveDto body) {
		ResponseDto<SpecialApproveDto> res = new ResponseDto<>();
		List<SpecialApproveDto> specialApproves = new ArrayList<>();
		List<SpecialApproveDto> listDto = new ArrayList<>();
		MemberDto member = memberService.getCurrentMember();
		SpecialApproveDto bodydto = new SpecialApproveDto();
		try {
			bodydto.setStatus(body.getStatus());

			for (long said : body.getSaId()) {
				System.out.println(said);
				bodydto.setSpecialApproveId(said);
				listDto.add(specialApproveService.update(bodydto, member.getId()));
			}
			if (body.getStatus().equals("1")) {
				for (SpecialApproveDto dto : listDto) {
					RegisterCourse(dto);
				}
			}
			if (listDto.isEmpty())
				throw new Exception("ไม่สามารถทำรายการได้");
			res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
			res.setData(listDto);
			res.setCode(200);
		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{courseId}")
	@PreAuthorize("hasRole('user')")
	public ResponseEntity<ResponseDto<SpecialApproveDto>> UserDelete(@PathVariable("courseId") Long courseId) {
		ResponseDto<SpecialApproveDto> res = new ResponseDto<>();
		List<SpecialApproveDto> specialApproves = new ArrayList<>();
		SpecialApproveDto dto;
		MemberDto member = memberService.getCurrentMember();

		try {
			dto = specialApproveService.delete(courseId, member.getId());
			ResSuccess(res, specialApproves, dto);

		} catch (Exception e) {
			res.setResult(ResponseDto.RESPONSE_RESULT.Fail.getRes());
			res.setErrorMessage(e.getMessage());
			res.setCode(200);
		}
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	private void ResSuccess(ResponseDto<SpecialApproveDto> res, List<SpecialApproveDto> specialApproves,
			SpecialApproveDto dto) throws Exception {
		if (dto == null)
			throw new Exception("ไม่สามารถทำรายการได้");
		specialApproves.add(dto);
		res.setResult(ResponseDto.RESPONSE_RESULT.Success.getRes());
		res.setData(specialApproves);
		res.setCode(200);
	}

	private boolean RegisterCourse(SpecialApproveDto dto) {
		return specialApproveService.approve(dto);
	}
}
