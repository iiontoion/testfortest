package com.cdgs.temple.repository;

import com.cdgs.temple.entity.TempSpecialApproveEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TempSpecialApproveRepository extends CrudRepository<TempSpecialApproveEntity, Long> {

    @Query(value = "SELECT sa.special_approve_id,sa.member_id,CONCAT(t.title_name,m.member_fname,' ',m.member_lname) as display_name FROM special_approve sa INNER JOIN courses_teacher ct ON sa.course_id=ct.course_id INNER JOIN members m ON sa.member_id=m.member_id INNER JOIN title_names t ON m.member_title_id=t.title_id WHERE sa.STATUS='2' AND ct.member_id=:memberId AND sa.course_id=:courseId", nativeQuery = true)
    List<TempSpecialApproveEntity> getAll(@Param("memberId") Long memberId, @Param("courseId") Long courseId);


}
