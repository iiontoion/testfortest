package com.cdgs.temple.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cdgs.temple.entity.LocationEntity;

@Repository
public interface LocationRepository extends CrudRepository<LocationEntity, Long> {
	List<LocationEntity> findAll();


}
