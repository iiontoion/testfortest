package com.cdgs.temple.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.cdgs.temple.entity.SpecialApproveEntity;

import java.util.List;

public interface SpecialApproveRepository extends CrudRepository<SpecialApproveEntity, Long> {

    List<SpecialApproveEntity> findAll();
    SpecialApproveEntity findBySpecialApproveId(Long saId);
    SpecialApproveEntity findByCourseIdAndMemberIdAndStatus(Long courseId, Long memberId, String status);

    @Query(value = "SELECT sa.* FROM special_approve sa LEFT JOIN courses_teacher ct ON sa.course_id=ct.course_id WHERE sa.status = '2' AND sa.special_approve_id=:saId AND ct.member_id=:memberId", nativeQuery = true)
    SpecialApproveEntity fetchBySaIdAndMemberId(Long saId, Long memberId);
    
	@Modifying
	@Transactional
	@Query(value = "UPDATE special_approve as sa " + 
			"SET sa.status = '3'   " + 
			"WHERE sa.course_id = :courseId   " + 
			"and sa.status = '2'",nativeQuery = true)
	void cancelApprove(@Param("courseId") Long id);

}
