package com.cdgs.temple.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class TempSpecialApproveEntity implements Serializable {
    private static final long serialVersionUID = -4780915762853262556L;

    @Id
    private long specialApproveId;
    private long memberId;
    private String displayName;

    public long getSpecialApproveId() {
        return specialApproveId;
    }

    public void setSpecialApproveId(long specialApproveId) {
        this.specialApproveId = specialApproveId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
