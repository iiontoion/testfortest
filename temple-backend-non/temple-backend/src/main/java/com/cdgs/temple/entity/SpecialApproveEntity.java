package com.cdgs.temple.entity;

import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "special_approve")
@Embeddable
public class SpecialApproveEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4780915762853262556L;
	
	@Id
	@Column(name = "special_approve_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long specialApproveId;
	
	@Column(name = "member_id")
	private Long memberId;
	
	@Column(name = "course_id")
	private Long courseId;
	
	@Column(name = "detail")
	private String detail;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "create_date")
	@CreationTimestamp
	private LocalDateTime createDate;
	
	@Column(name = "last_update")
	@CreationTimestamp
	private LocalDateTime lastUpdate;

	public Long getSpecialApproveId() {
		return specialApproveId;
	}

	public void setSpecialApproveId(Long specialApproveId) {
		this.specialApproveId = specialApproveId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
